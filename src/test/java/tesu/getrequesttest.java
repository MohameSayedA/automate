package tesu;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.shaft.api.RestActions;
import com.shaft.api.RestActions.ParametersType;
import com.shaft.api.RestActions.RequestType;
import com.shaft.driver.DriverFactory;
import com.shaft.validation.Assertions;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class getrequesttest {
	@Test
	public void intro() {

		Response apim = DriverFactory.getAPIDriver("http://api.zippopotam.us/")
				.buildNewRequest("us/90210", RequestType.GET).performRequest();
		String country = RestActions.getResponseJSONValue(apim, "country");
		Assertions.assertEquals("United States", country);

	}
}
