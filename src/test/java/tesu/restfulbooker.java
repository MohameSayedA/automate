package tesu;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.shaft.api.RestActions;
import com.shaft.api.RestActions.RequestType;
import com.shaft.driver.DriverFactory;
import com.shaft.validation.Verifications;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class restfulbooker {
	@Test
	public void getbokingid() {
		RestActions apiObject = DriverFactory.getAPIDriver("https://restful-booker.herokuapp.com/");
		apiObject.buildNewRequest("booking", RequestType.GET).performRequest();

	}

	@Test
	public void getboking() {
		RestActions apiObject = DriverFactory.getAPIDriver("https://restful-booker.herokuapp.com/");
		apiObject.buildNewRequest("booking/" + "1", RequestType.GET).performRequest();

	}

	@SuppressWarnings("unchecked")
	@Test
	public void createBooking() {
		RestActions apiObject = DriverFactory.getAPIDriver("https://restful-booker.herokuapp.com/");
		JSONObject createBookingBody = new JSONObject();
		JSONObject bookingDates = new JSONObject();
		createBookingBody.put("firstname", "Shimaa");
		createBookingBody.put("lastname", "Sayed");
		createBookingBody.put("totalprice", 1000);
		createBookingBody.put("depositpaid", true);
		bookingDates.put("checkin", "2020-01-01");
		bookingDates.put("checkout", "2021-01-01");
		createBookingBody.put("bookingdates", bookingDates);
		createBookingBody.put("additionalneeds", "IceCream");

		// Create new booking
		Response createBookingRes = apiObject.buildNewRequest("booking", RequestType.POST)
				.setRequestBody(createBookingBody).setContentType(ContentType.JSON).performRequest();
		
		String bookingId = RestActions.getResponseJSONValue(createBookingRes, "bookingid");

		// Get the created booking values
		Response getBookingRes = apiObject.buildNewRequest("booking/" + bookingId, RequestType.GET).performRequest();
		String firstName = RestActions.getResponseJSONValue(getBookingRes, "firstname");
		String lastName = RestActions.getResponseJSONValue(getBookingRes, "lastname");
		String checkin = RestActions.getResponseJSONValue(getBookingRes, "bookingdates.checkin");
		String checkout = RestActions.getResponseJSONValue(getBookingRes, "bookingdates.checkout");
		String totalprice = RestActions.getResponseJSONValue(getBookingRes, "totalprice");

		// Validations
		Verifications.verifyEquals("Shimaa", firstName);
		Verifications.verifyEquals("Sayed", lastName);
		Verifications.verifyEquals("2020-01-01", checkin);
		Verifications.verifyEquals("2021-01-01", checkout);
		

	}

}
